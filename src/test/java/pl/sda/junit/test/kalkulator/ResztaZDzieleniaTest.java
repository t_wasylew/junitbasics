package pl.sda.junit.test.kalkulator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static pl.sda.junit.kalkulator.Kalkulator.czyLiczbaPodzielnaPrzez;

public class ResztaZDzieleniaTest {

    @Test
    public void czteryPodzielnePrzezDwa() {
        assertTrue(czyLiczbaPodzielnaPrzez(4, 2));
    }

    @Test
    public void trzyNiePodzielnePrzezDwa() {
        assertFalse(czyLiczbaPodzielnaPrzez(3, 2));
    }

    @Test
    public void stoPodzielnePrzezSto() {
        assertTrue(czyLiczbaPodzielnaPrzez(100, 100));
    }
}
