package pl.sda.junit.test.kalkulator;

import org.junit.Test;
import pl.sda.junit.kalkulator.Kalkulator;

import static org.junit.Assert.assertEquals;

public class PotegaTest {
    @Test
    public void dwaDoDrugiejDajeCztery() {
        assertEquals(4, Kalkulator.potega(2, 2));
    }

    @Test
    public void stoDoZerowjDajeJeden() {
        assertEquals(1, Kalkulator.potega(100, 0));
    }

    @Test
    public void zeroDoSetnejDajeZero() {
        assertEquals(0, Kalkulator.potega(0, 100));
    }
}
