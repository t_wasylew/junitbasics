package pl.sda.junit.kalkulator;

public class Kalkulator {

    public static int potega(int podstawa, int wykladnik) {
        int wynik = 1;
        for (int i = 0; i < wykladnik; i++) {
            wynik *= podstawa;
        }
        return wynik;
    }

    public static boolean czyLiczbaPodzielnaPrzez(int liczba, int dzielnik) {
        return liczba % dzielnik == 0;
    }
}
